package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	// Get all Users
	@GetMapping("/users")
	public String getAllUsers(){
		return "All users retrieved";
	}

	// Create New User
	@PostMapping("/users")
	public String createUser(){
		return "New user created";
	}

	// Get a specific User
	@GetMapping("/users/{userId}")
	public String getUser(@PathVariable Long userId){
		return "User with userId: " + userId + " found!";
	}

	// Delete a user
	@DeleteMapping("/users/{userId}")
	public String deleteUser(@PathVariable Long userId ,@RequestHeader(value = "Authorization") String user){
		if(user.equals(null) || user.equals("")){
			return "Unauthorized Access";
		}else{
			return "User " + userId + " Has been deleted";
		}
	}

	//Update User
	@PutMapping("/users/{userId}")
	@ResponseBody
	public User updateUser(@PathVariable Long userId, @RequestBody User username){
		return username;
	}

}
